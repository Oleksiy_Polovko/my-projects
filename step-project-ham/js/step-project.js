const itemListServices = Array.from(document.body.querySelectorAll('.item-list-sevices'));

const serviceMenuItem = Array.from(document.body.querySelectorAll('.service-menu-item'));

const classActiveItemClear = (elem, className = 'item-active') => {
    elem.find(item => item.classList.remove(`${className}`));
};

const classActiveItemAdd = (elem, index, className = 'item-active') => {
    elem[index].classList.add(`${className}`);
};

const checkItems = (item, index) => {
    item.addEventListener('click', () => {
        console.log(item);

        classActiveItemClear(itemListServices);
        classActiveItemClear(serviceMenuItem);

        classActiveItemAdd(itemListServices, index);
        classActiveItemAdd(serviceMenuItem, index);
    });
};

itemListServices.forEach(checkItems);