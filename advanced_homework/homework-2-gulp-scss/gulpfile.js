const gulp = require("gulp");
const dartSass = require("sass");
const gulpSass = require("gulp-sass");
const browserSync = require('browser-sync');
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
/* import imagemin from 'gulp-imagemin'; */
const minifyjs = require('gulp-js-minify');
const uglifycss = require('gulp-uglifycss');
const concat = require('gulp-concat');
const rename = require('gulp-rename');


const BS = browserSync.create();
const sass = gulpSass(dartSass);

const buildStyles = () => gulp.src("./src/scss/main.scss").pipe(sass()).pipe(gulp.dest("./dist/css"));

exports.buildStyles = buildStyles;

const buildStylesMin = () => {
  gulp.src("./src/scss/main.scss").pipe(sass()).pipe(uglifycss({
    "maxLineLen": 80,
    "uglyComments": true
  })).pipe(rename('styles.min.css')).pipe(gulp.dest("./dist/css"))
};

exports.buildStylesMin = buildStylesMin;

const moveScript = () =>
  gulp.src("./src/script/**/*").pipe(gulp.dest("./dist/script"));

exports.moveScript = moveScript;

const minifyJs = () => {
  gulp.src('./src/script/**/*')
    .pipe(minifyjs())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./dist/script'));
};

exports.minifyJs = minifyJs;

const moveImages = () =>
  gulp.src("./src/img/**/*").pipe(gulp.dest("./dist/img"));

exports.moveImages = moveImages;

/* const autopref = () => {
  gulp.src('./src/scss/main.scss').pipe(autoprefixer({
    cascade: false
  }))
    .pipe(gulp.dest('./dist/css'))
}; */

const autopref = () => {
  gulp.src('./src/scss/**/*.scss').pipe(autoprefixer({
    cascade: false
  }))
    .pipe(gulp.dest('./src/scss'))
};

exports.autopref = autopref;

const cleanDist = () => {
  return gulp.src('dist', { read: false })
    .pipe(clean());
};

exports.cleanDist = cleanDist;

exports.dev = gulp.series(cleanDist, gulp.parallel(buildStyles, moveImages, moveScript, autopref, buildStylesMin, minifyJs, () => {
  BS.init({
    server: {
      baseDir: "./",
    },
  });

  gulp.watch(
    "./src/**/*",
    gulp.series(buildStyles, moveScript, (done) => {
      BS.reload();
      done();
    })
  );
}));

gulp.task('clean-dist', cleanDist);

//gulp.task('img-min', () => {
 // gulp.src('./src/img/**/*')
   // .pipe(imagemin())
    //.pipe(gulp.dest('./dist/img'))
//});
