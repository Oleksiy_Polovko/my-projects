function createCitiesList() {
       el = document.createElement('ul');
       document.body.appendChild(el);
       return el;
};

function createCitiesListItems(cities, parentList) {
       if (!parentList) {
              parentList = createCitiesList();
       }
       const citiesHtml = cities.map(item => `<li>${item}</li>`).join('');
       parentList.insertAdjacentHTML('afterbegin', citiesHtml);
};

const list = createCitiesList();
const cities = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
createCitiesListItems(cities, list); 

