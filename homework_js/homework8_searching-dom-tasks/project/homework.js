const paragraphs = document.getElementsByTagName('p');

for (let item of paragraphs) {
  item.style.backgroundColor = '#ff0000';
};

let optionsList = document.getElementById('optionsList');

console.log(optionsList);
console.log(optionsList.parentElement);
console.log(optionsList.childNodes);

let testParagraph = document.getElementById('testParagraph');

testParagraph.innerText = 'This is a paragraph';

let mainHeader = document.querySelectorAll('.main-header li');

mainHeader.forEach(elem => {
  elem.classList.add('nav-item');
});

console.log(mainHeader);


let sectionTitle = document.querySelectorAll('.section-title');

sectionTitle.forEach((item => {
  item.classList.remove('section-title');
}))

console.log(sectionTitle);

