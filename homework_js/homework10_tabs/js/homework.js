const tabsTitle = Array.from(document.body.querySelectorAll('.tabs-title'));

const tabsItem = Array.from(document.body.querySelectorAll('.tabs-item'));

const classActiveClear = (elem, className = 'active') => {
    elem.find(item => item.classList.remove(`${className}`));
};

const classActiveAdd = (elem, index, className = 'active') => {
    elem[index].classList.add(`${className}`);
};

const checkItems = (item, index) => {
    item.addEventListener('click', () => {
        console.log(item);

    classActiveClear(tabsTitle);
    classActiveClear(tabsItem);

    classActiveAdd(tabsTitle, index);
    classActiveAdd(tabsItem, index);
    });
};



tabsTitle.forEach(checkItems);